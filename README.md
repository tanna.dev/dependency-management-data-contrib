# Dependency Management Data (DMD) contrib

User-contributed configuration and snippets related to [Dependency Management Data (DMD)](https://dmd.tanna.dev).

These can be downloaded through [`dmd contrib download`](https://dmd.tanna.dev/commands/dmd_contrib_download/) and generally applied through [`dmd db generate`](https://dmd.tanna.dev/commands/dmd_db_generate/).

## Advisories (`advisories`)

Add custom advisories that can flag usage of unmaintained/deprecated dependencies, flag security issues, etc.

### Adding new advisories

Advisories are a concept in dependency-management-data that allow for flagging the usage of dependencies that may be deprecated, unmaintained, have security issues, or any other reason. They are described more in the blog post [_Custom Advisories: the unsung hero of dependency-management-data_](https://www.jvt.me/posts/2023/08/29/dmd-custom-advisories/).

The file structure in the `advisories` directory is based on the package manager that the advisory is for. If you're creating a new file, copy-paste the file's basic content from a different file, then add your package manager specific data.

For instance, if we want to flag the usage of a set of versions of the `jekyll` project, we would amend `advisories/bundler.sql` to add the following:

```diff
-)
+),
+(
+  'jekyll',
+  'bundler',
+  '4',
+  'LESS_THAN',
+  'OTHER',
+  'We prefer to use the latest version of Jenkyll'
+)
;
```

## License

Licensed under the Apache-2.0 license.
