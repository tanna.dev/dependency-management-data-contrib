INSERT INTO custom_advisories (
  package_pattern,
  package_manager,
  version,
  version_match_strategy,
  advisory_type,
  description
) VALUES
(
  'github.com/pkg/errors',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'pkg/errors was archived in 2021, and is unmaintained since'
),
(
  'github.com/pkg/errors',
  'gomod',
  NULL,
  NULL,
  'DEPRECATED',
  'pkg/errors is no longer necessary, as functionality exists in the Go standard library, or in better packages'
),
(
  'github.com/golang/mock',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'golang/mock is no longer maintained, and active development been moved to github.com/uber/mock'
),
(
  'github.com/golangci/lint-1',
  'gomod',
  NULL,
  NULL,
  'DEPRECATED',
  'Use golang.org/x/lint instead, as the golangci fork has not been updated in several years, and is behind active development and bugfixes in golang.org/x/lint'
),
(
  'github.com/AlecAivazis/survey',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'AlecAivazis/survey was archived in 2023, and is unmainatined since. The project suggests https://github.com/charmbracelet/bubbletea (not a drop-in replacement) as an alternative'
),
(
  'github.com/mitchellh/cli',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'The maintainer has noted (https://gist.github.com/mitchellh/90029601268e59a29e64e55bab1c5bdc) that this project is no longer maintained, and will be archived as of early 2024. The maintainer indicates that github.com/hashicorp/cli is the recommended migration path'
),
(
  'github.com/mitchellh/colorstring',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'The maintainer has noted (https://gist.github.com/mitchellh/90029601268e59a29e64e55bab1c5bdc) that this project is no longer maintained, and will be archived as of early 2024.'
),
(
  'github.com/mitchellh/copystructure',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'The maintainer has noted (https://gist.github.com/mitchellh/90029601268e59a29e64e55bab1c5bdc) that this project is no longer maintained, and will be archived as of early 2024.'
),
(
  'github.com/mitchellh/go-homedir',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'The maintainer has noted (https://gist.github.com/mitchellh/90029601268e59a29e64e55bab1c5bdc) that this project is no longer maintained, and will be archived as of early 2024.'
),
(
  'github.com/mitchellh/go-mruby',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'The maintainer has noted (https://gist.github.com/mitchellh/90029601268e59a29e64e55bab1c5bdc) that this project is no longer maintained, and will be archived as of early 2024.'
),
(
  'github.com/mitchellh/go-ps',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'The maintainer has noted (https://gist.github.com/mitchellh/90029601268e59a29e64e55bab1c5bdc) that this project is no longer maintained, and will be archived as of early 2024.'
),
(
  'github.com/mitchellh/go-server-timing',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'The maintainer has noted (https://gist.github.com/mitchellh/90029601268e59a29e64e55bab1c5bdc) that this project is no longer maintained, and will be archived as of early 2024.'
),
(
  'github.com/mitchellh/go-vnc',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'The maintainer has noted (https://gist.github.com/mitchellh/90029601268e59a29e64e55bab1c5bdc) that this project is no longer maintained, and will be archived as of early 2024.'
),
(
  'github.com/mitchellh/hashstructure',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'The maintainer has noted (https://gist.github.com/mitchellh/90029601268e59a29e64e55bab1c5bdc) that this project is no longer maintained, and will be archived as of early 2024.'
),
(
  'github.com/mitchellh/ioprogress',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'The maintainer has noted (https://gist.github.com/mitchellh/90029601268e59a29e64e55bab1c5bdc) that this project is no longer maintained, and will be archived as of early 2024.'
),
(
  'github.com/mitchellh/mapstructure',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'The maintainer has noted (https://gist.github.com/mitchellh/90029601268e59a29e64e55bab1c5bdc) that this project is no longer maintained, and will be archived as of early 2024. The maintainer indicates that github.com/go-viper/mapstructure is the recommended migration path'
),
(
  'github.com/mitchellh/panicwrap',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'The maintainer has noted (https://gist.github.com/mitchellh/90029601268e59a29e64e55bab1c5bdc) that this project is no longer maintained, and will be archived as of early 2024.'
),
(
  'github.com/mitchellh/pointerstructure',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'The maintainer has noted (https://gist.github.com/mitchellh/90029601268e59a29e64e55bab1c5bdc) that this project is no longer maintained, and will be archived as of early 2024.'
),
(
  'github.com/mitchellh/protoc-gen-go-json',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'The maintainer has noted (https://gist.github.com/mitchellh/90029601268e59a29e64e55bab1c5bdc) that this project is no longer maintained, and will be archived as of early 2024.'
),
(
  'github.com/mitchellh/reflectwalk',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'The maintainer has noted (https://gist.github.com/mitchellh/90029601268e59a29e64e55bab1c5bdc) that this project is no longer maintained, and will be archived as of early 2024.'
),
(
  'github.com/mitchellh/osext',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'The maintainer has deleted the project, after 6 years of noting it should not be depended upon (https://gist.github.com/mitchellh/90029601268e59a29e64e55bab1c5bdc?permalink_comment_id=4802235#gistcomment-4802235)'
),
(
  'go.elastic.co/apm',
  'gomod',
  NULL,
  NULL,
  'DEPRECATED',
  'As noted in https://github.com/elastic/apm-agent-go, Elastic have deprecated the Go APM agent, and are instead recommending the move over to the OpenTelemetry Go SDK, which provides similar functionality, but requires a migration (https://www.elastic.co/blog/elastic-go-apm-agent-to-opentelemetry-go-sdk)'
),
(
  'go.elastic.co/apm/%',
  'gomod',
  NULL,
  NULL,
  'DEPRECATED',
  'As noted in https://github.com/elastic/apm-agent-go, Elastic have deprecated the Go APM agent, and are instead recommending the move over to the OpenTelemetry Go SDK, which provides similar functionality, but requires a migration (https://www.elastic.co/blog/elastic-go-apm-agent-to-opentelemetry-go-sdk)'
)
;
