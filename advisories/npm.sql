INSERT INTO custom_advisories (
  package_pattern,
  package_manager,
  version,
  version_match_strategy,
  advisory_type,
  description
) VALUES
(
  'angular',
  'npm',
  '2',
  'LESS_THAN',
  'UNMAINTAINED',
  'For the actively supported Angular, see https://www.npmjs.com/package/@angular/core. AngularJS support has officially ended. For extended AngularJS support options, see https://goo.gle/angularjs-path-forward.'
),
(
  'xlsx',
  'npm',
  NULL,
  NULL,
  'UNMAINTAINED',
  'The maintainers of the package no longer distribute the package to npm, which means updates will not be available. See also https://github.com/SheetJS/sheetjs/issues/2667 and https://github.com/SheetJS/sheetjs/issues/2825'
),
(
  'xlsx',
  'npm',
  '0.19.3',
  'LESS_THAN',
  'SECURITY',
  'The maintainers of the package no longer distribute the package to npm, which means CVE-2023-30533 exists in your version, but it is not trivial to remediate it. See also https://github.com/SheetJS/sheetjs/issues/2667 and https://github.com/SheetJS/sheetjs/issues/2825'
)
;
