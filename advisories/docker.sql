INSERT INTO custom_advisories (
  package_pattern,
  package_manager,
  version,
  version_match_strategy,
  advisory_type,
  description
) VALUES
(
  'public.ecr.aws/lambda/go',
  'docker',
  '1',
  'EQUALS',
  'DEPRECATED',
  'Amazon does not recommend the use of the v1 Go image, which is based off of Amazon Linux (v1) https://docs.aws.amazon.com/lambda/latest/dg/go-image.html#go-image-v1'
);
